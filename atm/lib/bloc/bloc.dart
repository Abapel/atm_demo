import 'dart:math';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'event.dart';
part 'state.dart';

class AtmBloc extends Bloc<AtmEvent, AtmState> {
  AtmBloc() : super(AtmState.withRandomLimits());

  @override
  Stream<AtmState> mapEventToState(AtmEvent event) async* {
    if (event is NewValue)
      yield AtmState(value: event.value, limits: state.limits);
    if (event is Withdraw) yield* _withdrawCash();
  }

  Stream<AtmState> _withdrawCash() async* {
    var request = state.value;
    final result = <int, int>{};
    final newLimits = _sortedLimits;
    //print('Before=$newLimits');
    newLimits.forEach((key, value) {
      final count = min(request ~/ key, value);
      newLimits[key] = value - count;
      request -= count * key;
      result[key] = count;
    });
    //print('After=$newLimits');
    //print('Result=$result');
    if (result.totalBalance == state.value)
      yield Withdrawed(data: result, limits: newLimits, value: state.value);
    else
      yield state.failed;
  }

  Map<int, int> get _sortedLimits {
    final result = <int, int>{};
    state.limits.keys.toList()
      ..sort((a, b) => b.compareTo(a))
      ..forEach((key) {
        result[key] = state.limits[key];
      });
    return result;
  }
}

extension _Sum on Map<int, int> {
  int get totalBalance {
    int balance = 0;
    forEach((key, value) {
      balance += key * value;
    });
    return balance;
  }
}
