part of 'bloc.dart';

abstract class AtmEvent extends Equatable {
  const AtmEvent();

  @override
  List<Object> get props => [];
}

class NewValue extends AtmEvent {
  const NewValue(this.value);
  final int value;

  @override
  List<Object> get props => [value];
}

class Withdraw extends AtmEvent {
  const Withdraw();
}
