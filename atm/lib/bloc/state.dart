part of 'bloc.dart';

class AtmState extends Equatable {
  const AtmState({this.value = 0, this.limits});

  final Map<int, int> limits;
  final int value;

  @override
  List<Object> get props => [value, limits];

  Failed get failed => Failed(value: value, limits: limits);

  factory AtmState.withRandomLimits() {
    final r = Random();
    return AtmState(
      value: 0,
      limits: <int, int>{
        100: r.nextInt(99),
        200: r.nextInt(99),
        500: r.nextInt(99),
        1000: r.nextInt(99),
        2000: r.nextInt(99),
        5000: r.nextInt(99),
      },
    );
  }
}

class Withdrawed extends AtmState {
  const Withdrawed({this.data, int value, Map<int, int> limits})
      : super(limits: limits, value: value);

  final Map<int, int> data;

  @override
  List<Object> get props => [value, data, limits];
}

class Failed extends AtmState {
  const Failed({int value, Map<int, int> limits})
      : super(value: value, limits: limits);
}
