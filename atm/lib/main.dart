import 'package:atm/bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:wave/config.dart';
import 'package:wave/wave.dart';

void main() {
  runApp(AtmApp());
}

class AtmApp extends StatelessWidget {
  const AtmApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.white,
        appBar: GradientAppBar(
          title: Row(
            children: <Widget>[
              SizedBox(
                width: 30,
                height: 30,
                child: Image.asset('images/logo.png'),
              ),
              const SizedBox(width: 2),
              Text('ATM'),
            ],
          ),
          gradient: LinearGradient(colors: [_blue, _purple]),
        ),
        body: BlocProvider<AtmBloc>(
          create: (_) => AtmBloc(),
          child: LayoutBuilder(
            builder: (_, constrants) =>
                constrants.maxHeight > constrants.maxWidth
                    ? const _PortraitPage()
                    : const _LandscapePage(),
          ),
        ),
      ),
    );
  }
}

class _PortraitPage extends StatelessWidget {
  const _PortraitPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        const _TextInput(),
        const _WithdrawButton(),
        const _Divider(),
        const _AtmResult(),
        const _Divider(),
        const _AtmBalance(),
        const _Divider(),
        const _Footer(),
      ],
    );
  }
}

class _LandscapePage extends StatelessWidget {
  const _LandscapePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Flexible(
          flex: 1,
          child: Column(children: <Widget>[
            const _AtmResult(),
            const _Divider(),
            const _AtmBalance(),
          ]),
        ),
        const _Divider(vertical: true),
        Flexible(
          flex: 1,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              const _TextInput(),
              const _WithdrawButton(),
              const _Footer(),
            ],
          ),
        ),
      ],
    );
  }
}

class _TextInput extends StatelessWidget {
  const _TextInput({Key key}) : super(key: key);

  static const _style = TextStyle(
    color: Colors.white,
    fontSize: 30.0,
    fontWeight: FontWeight.w400,
  );

  @override
  Widget build(BuildContext context) {
    return Flexible(
      flex: 1,
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          RotatedBox(
            quarterTurns: 2,
            child: _Waves(upsideDown: true),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Flexible(
                flex: 1,
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    'Введите сумму',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 15.0,
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 2),
              Flexible(
                flex: 2,
                child: TextField(
                  inputFormatters: [
                    TextInputFormatter.withFunction(_formatText)
                  ],
                  controller: TextEditingController(
                    text: '${context.bloc<AtmBloc>().state.value}.00 руб',
                  ),
                  keyboardType: TextInputType.number,
                  textAlign: TextAlign.center,
                  onChanged: (text) {
                    var index = text.indexOf('.');
                    if (index != -1) text = text.substring(0, index);
                    final value = int.tryParse(text) ?? 0;
                    context.bloc<AtmBloc>().add(NewValue(value));
                  },
                  style: _style,
                  decoration: InputDecoration.collapsed(
                    hintText: '0',
                    hintStyle: _style,
                  ),
                ),
              ),
              const SizedBox(height: 2),
              Container(
                color: Colors.white.withOpacity(0.3),
                height: 1,
                width: MediaQuery.of(context).size.width / 3,
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class _WithdrawButton extends StatelessWidget {
  const _WithdrawButton({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flexible(
      flex: 1,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 16),
        child: BlocBuilder<AtmBloc, AtmState>(
          buildWhen: (previous, current) => previous.value != current.value,
          builder: (_, state) => AnimatedOpacity(
            opacity: state.value > 0 ? 1.0 : 0.5,
            duration: const Duration(milliseconds: 100),
            child: FlatButton(
              color: const Color(0xFFE61EAD),
              shape: const StadiumBorder(),
              child: Padding(
                padding: const EdgeInsets.all(16),
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    'Выдать сумму',
                    style: const TextStyle(fontSize: 16, color: Colors.white),
                  ),
                ),
              ),
              onPressed: () {
                if (state.value > 0) context.bloc<AtmBloc>().add(Withdraw());
              },
            ),
          ),
        ),
      ),
    );
  }
}

class _AtmResult extends StatelessWidget {
  const _AtmResult({Key key}) : super(key: key);

  Widget _buildError() {
    return Center(
      child: FittedBox(
        fit: BoxFit.scaleDown,
        child: Text(
          'Банкомат не может выдать\nзапрашиваемую сумму',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 18.0,
            color: const Color(0xFFE61EAD),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Flexible(
      flex: 1,
      child: BlocBuilder<AtmBloc, AtmState>(
        builder: (_, state) {
          if (state is Failed) return _buildError();
          if (state is Withdrawed)
            return _DataTable(
              title: 'Банкомат выдал следующие купюры',
              data: state.data,
            );
          return const SizedBox.expand();
        },
      ),
    );
  }
}

class _AtmBalance extends StatelessWidget {
  const _AtmBalance({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flexible(
      flex: 1,
      child: BlocBuilder<AtmBloc, AtmState>(
        buildWhen: (previous, current) =>
            !mapEquals(previous.limits, current.limits),
        builder: (_, state) => _DataTable(
          title: 'Баланс банкомата',
          data: state.limits,
        ),
      ),
    );
  }
}

class _DataTable extends StatelessWidget {
  const _DataTable({Key key, this.title, this.data}) : super(key: key);

  final String title;
  final Map<int, int> data;

  Widget _buildValueText(String text) {
    return Expanded(
      child: FittedBox(
        fit: BoxFit.scaleDown,
        alignment: Alignment.centerLeft,
        child: Text(
          text,
          style: const TextStyle(
            fontSize: 14,
            color: Color(0xFF3827B4),
          ),
        ),
      ),
    );
  }

  Widget _buildRow(double height, List<Widget> children) {
    return SizedBox(
      height: height,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: children,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (_, constraints) {
      final _rowHeight = (constraints.maxHeight - 24) / 4;
      return Container(
        padding: const EdgeInsets.all(8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _buildRow(_rowHeight, <Widget>[
              FittedBox(
                fit: BoxFit.scaleDown,
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    title,
                    style: TextStyle(fontSize: 13, color: Color(0xFFA3A2AC)),
                  ),
                ),
              ),
            ]),
            const SizedBox(height: 4),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                children: <Widget>[
                  _buildRow(_rowHeight, <Widget>[
                    _buildValueText('${data[100]} X 100 рублей'),
                    _buildValueText('${data[500]} X 500 рублей'),
                  ]),
                  const SizedBox(height: 2),
                  _buildRow(_rowHeight, <Widget>[
                    _buildValueText('${data[200]} X 200 рублей'),
                    _buildValueText('${data[1000]} X 1000 рублей'),
                  ]),
                  const SizedBox(height: 2),
                  _buildRow(_rowHeight, <Widget>[
                    _buildValueText('${data[2000]} X 2000 рублей'),
                    _buildValueText('${data[5000]} X 5000 рублей'),
                  ]),
                ],
              ),
            ),
          ],
        ),
      );
    });
  }
}

class _Divider extends StatelessWidget {
  const _Divider({Key key, this.vertical = false}) : super(key: key);

  final bool vertical;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: vertical ? null : 12,
      width: vertical ? 12 : null,
      color: const Color(0x0F3827B4),
    );
  }
}

class _Footer extends StatelessWidget {
  const _Footer({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flexible(
      flex: 1,
      child: Padding(
        padding: const EdgeInsets.only(top: 16),
        child: const _Waves(),
      ),
    );
  }
}

class _Waves extends StatelessWidget {
  const _Waves({Key key, this.upsideDown = false}) : super(key: key);

  final bool upsideDown;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (_, constraints) => WaveWidget(
        config: CustomConfig(
          gradients: [
            [_blue.withOpacity(0.7), _purple.withOpacity(0.7)],
            [_blue.withOpacity(0.7), _purple.withOpacity(0.7)],
            [_blue.withOpacity(0.4), _purple.withOpacity(0.4)],
            [_blue.withOpacity(0.2), _purple.withOpacity(0.2)],
          ],
          durations: [10000, 12000, 10000, 9000],
          heightPercentages: List.filled(4, 0.05),
          gradientBegin:
              upsideDown ? Alignment.centerRight : Alignment.centerLeft,
          gradientEnd:
              upsideDown ? Alignment.centerLeft : Alignment.centerRight,
        ),
        size: Size(double.infinity, double.infinity),
        waveAmplitude: constraints.maxHeight * 0.05,
      ),
    );
  }
}

TextEditingValue _formatText(
    TextEditingValue oldValue, TextEditingValue newValue) {
  var text = newValue.text;
  var index = text.indexOf('.');
  if (index == -1) index = 0;
  text = text.substring(0, index);
  text = text.replaceAll(',', '');

  final offset = newValue.selection.baseOffset;
  if (newValue.text.length > oldValue.text.length && offset > index) {
    final char = newValue.text.substring(offset - 1, offset);
    if (char != '.' && char != ',') {
      print('char=$char');
      text = '$text$char';
      index += 1;
    }
  }

  return TextEditingValue(
    text: '$text.00 руб',
    selection: TextSelection.collapsed(offset: index),
  );
}

const _blue = Color(0xFF3827B4);
const _purple = Color(0xFF6C18A4);
